#include <algorithm>
#include <cctype>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

using namespace std;

struct variables {
    string name;
    string value;
    int replacements = 0;
};

// Info if there is an invalid number of parameters
void show_info() {
    cout << "CSS VARIABLES REPLACEMENT" << endl
         << endl
         << "USAGE"
         << endl
         << "  css <css-file> <css-file w/ variables> <output filename> [optional]" << endl
         << endl
         << "REQUIRED PARAMETERS" << endl
         << "  <css-file>" << endl
         << "    file in which the css variables are used" << endl
         << endl
         << "  <css-file w/ variables>" << endl
         << "    file in which the css variables are declared" << endl
         << endl
         << "  <output filename>" << endl
         << "    name of the file to which the output should be written" << endl
         << endl
         << "OPTIONAL PARAMETERS" << endl
         << "  [optional]" << endl
         << "    -n | --nested        replace nested variables" << endl
         << "    -r | --replacements  show number of replacements" << endl
         << "    -v | --variables     show variables which are found in variables file" << endl
         << endl
         << "Clustered parameters should be in alphabetical order" << endl
         << endl
         << "Made by Arno van Liere - 2019" << endl;
}

// Trim spaces at both ends of a string
string trim(string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(),
                                    not1(ptr_fun<int, int>(isspace))));
    s.erase(find_if(s.rbegin(), s.rend(),
                         not1(ptr_fun<int, int>(isspace))).base(), s.end());
    return s;
}

// Return maximal length of all array items
int max_length_item(int index, int length, variables *array) {
    int max_length = 0;
    for (int i = 0; i < length; i++) {
        if (index == 0) {
            if (array[i].name.length() > max_length)
                max_length = array[i].name.length();
        } else if (index == 1) {
            if (array[i].value.length() > max_length)
                max_length = array[i].value.length();
        }
    }
    return max_length;
}

// Replace variables in 'contents' with the values from 'variables_values'
// Write output to 'outfile'
void replace_variables(string contents, char *outfile, int number_variables, variables *variables_values) {
    string line, variable, value;
    int number_replacements = 0;
    unsigned long long int pos;

    ofstream fileout(outfile);
    for (int i = 0; i < number_variables; i++) {
        number_replacements = 0;
        variable = "var(" + variables_values[i].name + ")";
        value = variables_values[i].value;
        // Find first occurrence of the variable
        pos = contents.find(variable);
        while (pos != string::npos) {
            number_replacements++;
            // Replace 'variable' string with 'value'
            contents.replace(pos, variable.length(), value);
            // Continue searching from here
            pos = contents.find(variable, pos);
        }
        variables_values[i].replacements = number_replacements;
    }
    fileout << contents;
}

// If 'save' is true, variable names and values from 'file_contents' are saved in 'variables_values'
// 'save' should only be true if 'variables_values' has been initialized
// If 'save' is false, the number of variables is counted and saved in 'number_variables'
bool read_variables(char *filename, variables *variables_values, int &number_variables, string file_contents, bool save) {
    char input_char, prev_char = '\0', next_char;
    unsigned long long file_size = file_contents.size() - 1;
    string variable_name, variable_value;
    int variable = 0, index = 0;

    input_char = file_contents[index];
    while (index != file_size) {
        next_char = file_contents[index + 1];
        // Variable-names start with '--' and end with a ':'
        // 'prev_char' should be a space, tab or linebreak to prevent
        // variable use to be recognised as variable declaration
        if (input_char == '-' && next_char == '-' &&
            (prev_char == ' ' || prev_char == '\n' || prev_char == '\t')) {
            // Reset 'variable_name' and 'variable_value'
            variable_name = "";
            variable_value = "";
            while (input_char != ':' && index != file_size) {
                variable_name += input_char;
                index++;
                input_char = file_contents[index];
            }
            // Skip all spaces and the ':' after 'variable_name'
            while ((input_char == ' ' || input_char == ':') && index != file_size) {
                index++;
                input_char = file_contents[index];
            }
            // Only after a variable-name follows a variable-value
            // Variable-value ends with ';'
            while (input_char != ';' && index != file_size) {
                variable_value += input_char;
                index++;
                input_char = file_contents[index];
            }
            // Save names and values in array
            if (save && variables_values != nullptr) {
                // Trim spaces from variable_name and variable_value
                // and save those trimmed strings in array
                variables_values[variable].name = trim(variable_name);
                variables_values[variable].value = trim(variable_value);
            } else { // If 'save' is false, count the variables
                number_variables++;
            }
            // Increase counter for the variables
            variable++;
        }
        // Increase index for reading the file-contents
        index++;
        prev_char = input_char;
        input_char = file_contents[index];
    }

    if (!save && number_variables == 0) {
        cout << "ERROR: No variables found in \"" << filename << "\"" << endl;
        return false;
    } else if (!save) {
        cout << number_variables << " variables found in \"" << filename << "\"" << endl << endl;
    }
    return true;
}

// Get content from 'filename' en save it in 'contents'
// Returns true if it succeeds, false otherwise
bool get_contents(char *filename, string &contents) {
    ifstream file(filename, ios::in);
    if (file.fail()) {
        cout << "ERROR: \"" << filename << "\" could not be opened" << endl;
        return false;
    }
    contents.assign((istreambuf_iterator<char>(file)),
                    (istreambuf_iterator<char>()));
    return true;
}

bool parse_argument(int argc, char *argv[], string arguments[]) {
    bool parameter = false;
    // For-loop until '4' because each array with arguments is
    // 4 elements long
    for (int i = 0; i < 5; i++) {
        if ((argc >= 5 && (argv[4] == arguments[i])) ||
            (argc >= 6 && (argv[5] == arguments[i])) ||
            (argc >= 7 && (argv[6] == arguments[i]))) {
            parameter = true;
        }
    }
    return parameter;
}

int main(int argc, char *argv[]) {
    char *input_file, *input_variables_file, *output_file;
    string input_contents, variable_contents;
    variables *variable_values;
    int number_variables = 0, name_width, value_width;
    bool input_success, variable_read_success, variables_success;
    bool show_replacements, show_variables, replace_nested_vars;
    string nested_args[5] = {"--nested", "-n", "-nr", "-nv", "-nrv"};
    string replacements_args[5] = {"--replacements", "-r", "-nr", "-rv", "-nrv"};
    string vars_args[5] = {"--variables", "-v", "-nv", "-rv", "-nrv"};

    // Check if there are enough arguments
    if (argc < 4) {
        show_info();
        return 1;
    }
    
    // Parse arguments
    replace_nested_vars = parse_argument(argc, argv, nested_args);
    show_replacements = parse_argument(argc, argv, replacements_args);
    show_variables = parse_argument(argc, argv, vars_args);

    // Get filenames
    input_file = argv[1];
    input_variables_file = argv[2];
    output_file = argv[3];

    // Get file contents, if 'input_success' is false,
    // an error is already displayed, so close the program
    // Save contents in 'input_contents'
    input_success = get_contents(input_file, input_contents);
    if (!input_success) return 1;
    // Get file contents, if 'variable_success' is false,
    // an error is already displayed, so close the program
    // Save contents of variables-file in 'variable_contents'
    variable_read_success = get_contents(input_variables_file, variable_contents);
    if (!variable_read_success) return 1;

    // Count the variables in 'input_variables_file' and save number in 'number_variables'
    variables_success = read_variables(input_variables_file, nullptr, number_variables, variable_contents, false);
    // If 'variable_success' is false, an error is already displayed, so close the program
    if (!variables_success) return 1;

    // Create array with the length of the number of variables
    // to store the names, values and number of replacements
    variable_values = new variables[number_variables];

    // Save the variable names and values from 'variable_contents' in 'variable_values'
    read_variables(input_variables_file, variable_values, number_variables, variable_contents, true);

    if (replace_nested_vars) {
        // Replace the variables in 'variable_contents' with the values from 'variable_values'
        // and save the result in 'input_variables_file'
        replace_variables(variable_contents, input_variables_file, number_variables, variable_values);
        // Create array to save variables without nested variables in
        variable_values = new variables[number_variables];
        // Empty string with contents of variable-file
        variable_contents = '\0';
        // Read new contents of the variable-file
        get_contents(input_variables_file, variable_contents);
        // Save the variable values in the new array
        read_variables(input_variables_file, variable_values, number_variables, variable_contents, true);
    }

    // Replace variable names in 'input_contents' with variable values from 'variable_values'
    // Write output to 'output_file'
    replace_variables(input_contents, output_file, number_variables, variable_values);

    if (show_variables || show_replacements) {
        // Make the columns 4 wider than max width
        name_width = max_length_item(0, number_variables, variable_values) + 4;
        value_width = max_length_item(1, number_variables, variable_values) + 4;
        if (!show_variables) { // Just show variables with replacements
            cout << "The following variables were replaced in \"" << input_variables_file << "\"" << endl;
            cout << left << setw(name_width) << "Name" << "Replacements" << endl;
            for (int k = 0; k < number_variables; k++) {
                cout << left << setw(name_width) << variable_values[k].name << variable_values[k].replacements << endl;
            }
        } else if (!show_replacements) { // Just show variables with values
            cout << "The following variables were found in \"" << input_variables_file << "\"" << endl;
            cout << left << setw(name_width) << "Name" << left << setw(value_width) << "Value" << endl;
            for (int k = 0; k < number_variables; k++) {
                cout << left << setw(name_width) << variable_values[k].name << left << setw(value_width)
                     << variable_values[k].value << endl;
            }
        } else { // Show names, values and replacements
            cout << "The following variables were found and replaced in \"" << input_variables_file << "\"" << endl;
            cout << left << setw(name_width) << "Name" << left << setw(value_width) << "Value" <<
                 "Replacements" << endl;
            for (int k = 0; k < number_variables; k++) {
                cout << left << setw(name_width) << variable_values[k].name << left << setw(value_width)
                     << variable_values[k].value << variable_values[k].replacements << endl;
            }
        }
        cout << endl;
    }

    cout << "CSS-file saved as \"" << output_file << "\"" << endl;

    return 0;
}