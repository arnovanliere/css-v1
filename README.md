# IMPORTANT!
A new version of this is under development and can be found 
<a href="https://gitlab.com/arnovanliere/css">here</a>.

# CSS variables replacement
Because Internet Explorer and a lot of older versions of current 
browsers don't support CSS variables, it is recommended to include a stylesheet 
without variables. But manually replacing variables would be way to much work, 
so this tool replaces the variables for you. The tool needs separate files for
the variable declaration and the variable use. 

## Limitations
The program can only replace variables which are saved in a separate file and 
are declared under `:root`. Local scope variables in your 'main.css' are not
replaced. 

## Browser support
This program works with any browser as long as the values of your CSS-variables
work with the particular browser.

# Windows
To use this program on Windows, you should download 'cmake-build-release/css.exe'
and the 3 DLL-files. After that, place those 4 files in the folder with your
CSS-files and execute the program with Command Prompt. To get help, execute
`css -h` or `css -help`.

# Linux
To use this program on Linux, you should download the files in 'linux-build' and
navigate to the folder and execute `./css main.css variables.css ie.css`. 
Optional parameters can be found by executing `./css -h` or `./css -help`
If `./css` does not work and you get an error like 'Permission Denied'
you need to compile the program yourself. This can be done by moving to the folder
with 'main.cpp' and executing `g++ -std=c++11 main.cpp -o css` when using
the GCC compiler.

# Example
In 'cmake-build-debug', 'cmake-build-release' and 'linux-build' can example 
CSS-files be found. In this case, 'main.css' and 'variables.css' were manually 
created and 'ie.css' was made by executing `css main.css variables.css ie.css`.
The first argument is expected to be the file with the variable use, the second 
to be the file with the variable declaration en the third is the name of the fil
to which the generated CSS should be written. Optional parameters can be found by
executing `css -h` or `css -help`.

## Nested variables
**main.css**
```
input {
    box-shadow: var(--box-shadow-input);
}
```
**variables.css**
```
:root {
    --main-color-rgb: 85, 85, 85;
    --box-shadow-input: 0 0 8px rgba(var(--main-color-rgb), 0.4);
}
```
In case of the two files above, the command `css main.css variables.css ie.css`
would result in the following file. <br> <br>
**ie.css**
```
input {
    box-shadow: 0 0 8px rgba(var(--main-color-rgb), 0.4);
}
```
In order to replace the variables in 'variables.css' before replacing the variables
in 'main.css', the option `-n` can be added. So the command `css main.css variables.css ie.css -n`
would result in the following file. <br><br>
**ie.css**
```
input {
    box-shadow: 0 0 8px rgba(85, 85, 85, 0.4);
}
```